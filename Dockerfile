FROM python:3.11-alpine

COPY --from=golang:1.22-alpine /usr/local/go/ /usr/local/go/

WORKDIR /tmp/build
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
ENV PATH="/usr/local/go/bin:${PATH}"
